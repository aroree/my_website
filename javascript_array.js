//ARRAYS
//Arrays are JavaScript's way of making lists. These lists can store different data types and they are ordered
//helps organize and store data

var bucketList = ['Rappel into a cave', 'Take a falconry class', 'Learn to juggle'];

var bucketList = [1,2,3];
console.log(bucketList);


//SELECT ONE ITEM FROM THE ARRAY
//vaScript counts starting from 0, not 1, so the first item in an array will be at position 0. This is because JavaScript is zero-indexed

var bucketList = [1,2,3]; 
var listItem = bucketList[0]; //first Item
var listItem = bucketList[0]; //second item
console.log(listItem); 


// You can also access each individual character in a string the same way you do with arrays

var hello = 'Hello World'; 
console.log(hello[6]); 
// Output: W


//LENGTH OF AN ARRAY
//.length shows how many items are in an array
//.length is also a property for strings
var bucketList = ['Rappel into a cave', 'Take a falconry class']; 
console.log(bucketList.length); 
// Output: 2


//PUSH()-FUNCTION 
// It connects to bucketList with a period.
// Then we call it like a function. That's because push() is a function and one that JavaScript allows us to use right on an array.


var bucketList = ['item 0', 'item 1', 'item 2'];

bucketList.push('item 3', 'item 4'); 
//afterwards array will look like: ['item 0', 'item 1', 'item 2', 'item 3', 'item 4'];


//POP()-FUNCTION
// it deletes the last item of an array

var bucketList = ['item 0', 'item 1', 'item 2']; 
bucketList.pop(); 
console.log(bucketList); 
// Output: [ 'item 0', 'item 1' ]


