//FUNCTIONS
// A function is a block of code designed to perform a task.

var calculatorOn = false;

function pressPowerButton() {
  if (calculatorOn) {
    console.log('Calculator turning off.');
    calculatorOn = false;
  } else {
    console.log('Calculator turning on.');
    calculatorOn = true;
  }
}

pressPowerButton();
// Output: Calculator turning on.

pressPowerButton();
// Output: Calculator turning off.

//FUNCTION DECLARATION

function getTax(){}


//PARAMETERS AND FUNCTIONS
// Parameters are variables that we can set when we call the function.
//Note on terminology: inputNumber is a parameter, but when we call multiplyByThirteen(9), the 9 is called an argument. Therefore, arguments are provided when you call a function, and parameters receive arguments as their value. So, inputNumber is a parameter and its value is the argument 9, since we wrote multiplyByThirteen(9).
//When calling a function, we can pass in arguments, which will set the function's parameters.

var calculatorOn = false;

function pressPowerButton() {
  if (calculatorOn) {
    console.log('Calculator turning off.');
    calculatorOn = false;
  } else {
    console.log('Calculator turning on.');
    calculatorOn = true;
  }
}

pressPowerButton();
// Output: Calculator turning on.

pressPowerButton();
// Output: Calculator turning off.



//set as many parameters as we'd like by adding them when we declare the function, separated by commas

function getRemainder(numberOne, numberTwo) {
  console.log(numberOne % numberTwo);
}

getRemainder(365, 27);
// Output: 14


//RETURN

function getRemainder(numberOne, numberTwo) {
  return numberOne % numberTwo;
}

console.log(getRemainder(365, 27));
// Output: 14


//RETURN WITH MORE FUNCTIONS
//Writing functions can help take large and difficult problems and break them into small and manageable problems.
//We can use return to return the result of a function which allows us to call functions anywhere, even inside other functions.

function multiplyByNineFifths(celsius) { 
	return celsius * (9/5); 
} 
function getFahrenheit(celsius) { 
	return multiplyByNineFifths(celsius) + 32; 
} 

console.log('The temperature is ' + getFahrenheit(15) + '°F'); 
// Output: The temperature is 59°F


