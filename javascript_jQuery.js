//JAVASCRIPT WITH HTML AND CSS AND JQUERY
//jQuery is a library to help JavaScript interact with HTML elements

//LINK JAVASCRIPT IN A HTML_FILE

<script src='js/main.js'></script>


//ALERT
//is a function that will create a pop-up window with text inside it

alert('Hello Javascript!');


//INTERACT WITH DOM
//DOM is the term for elements in an html file
//Elements are any html code denoted by html tags, like <div>, <p>,..

//interact by selecting its class attribute
var header = document.getElementsByClassName('example-class-name');

//This would find an element like this in the HTML:

<div class='example-class-name'> ... </div>


//JQUERY
// it's a library, which means it is a set of code in a file, therefore we will need to link that file in our HTML in order to access it

//Once linked, we'll need to make sure our HTML is loaded before we run our jQuery and JavaScript code --> therefore place it above the html src!

<script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>


//BUILT IN FUNCTIONS JQUERY

function main(){}
$(document).ready(main); //checks if the page is ready before it will run our code
//it's a callback function that's waiting until main (document) is started or DOM is ready


//SELECT .getELementsByClassName WITH JQUERY

var $skillset = $('.skillset');
var $skillset = $('#id-name-here');

function main() {
  var $skillset = $('.skillset');
  alert($skillset);
}

/*
    We can wrap any CSS selector, like class, id, or tag, with $('.example-class') to select it with jQuery.

    The selectors jQuery uses are the exact same as CSS selectors. For instance, if there's an element with a class of supporting-text, you could select it with $('.supporting-text'). Another example, if an element had an id of 'header', you could select it with $('#header').
*/

//HIDE 
//hide selected elements of the website

$('.my-selector').hide();

    /* We attached the hide function directly to the jQuery selector.
    The hide function will add the CSS property display: none to the DOM element from the page, which will hide it.
*/

//FADEIN
//lets fade the website when loaded
$('.example-class').fadeIn(400);

   /* We must start with an element that is not currently displayed on the page.
    Just like before, we can attach fadeIn directly to a jQuery selector.
    Within fadeIn's parentheses, we can specify how long we want the fade to last in milliseconds. 400 is the default.
    The example code will fade in the '.example-class' element over 0.4 seconds.
*/


//EVENT LISTENER ON CLICK
//This function will wait for a click event, and when one occurs, it will execute a provided function

$('.example-class').on('click', function() {
  // execute the code here when .example-class is clicked.
});
/*
   - $('.example-class') selects an HTML element with the class example-class.
   -.on('click', function() { ... }) adds a click listener to the selector. When it's clicked the function will execute the code within its block.
*/


//SHOW
//opposite of hide 

$('.example-class').show();
/*
    show is attached directly to the jQuery selector.
    show will change the CSS attribute display: none to a visible display property, therefore showing the element.
*/
//example
function main() {
 $('.skillset').hide();
  $('.skillset').fadeIn(1000);
 // $('.projects').hide();
  $('.projects-button').on('click',function(){
     $('.projects').show();
  });
  
}

$(document).ready(main);


//TOGGLE
// will hide or show an element, each time it is triggered

$('example-class').toggle();

/*    toggle can be called directly on an jQuery selector.
    When toggle is executed, it will hide or show the element that the selector points to. If the element is currently hidden, toggle will show the element, and vice versa.
*/

function main() {
 $('.skillset').hide();
  $('.skillset').fadeIn(1000);
 // $('.projects').hide();
  $('.projects-button').on('click',function(){
     $('.projects').toggle();
  });
  
}

$(document).ready(main);


//TOGGLECLASS
//We can toggle a CSS class with a jQuery function named toggleClass

$('.example-class').toggleClass('active');

  /*  .toggleClass is a function that will toggle a CSS class on the jQuery selector it's connected to. If the element has the class applied to it, toggleClass will remove it, and if the element does not have the class, it will add it.

    'active' is the class that we will toggle on and off. Notice that toggleClass does not require us to include the period before 'active' since it's already expecting a CSS class.
*/

//In css/styles.css, there is this class:
//The .active class will make the projects-button's background dark and its text light

.active {
  background-color: #333333;
  color: whitesmoke;
}


//THIS
//We can select the specific element we clicked on with the jQuery selector $(this)

$('.example-class').on('click', function() {
  $(this).toggleClass('active');
});

 /*  $(this) selects the clicked element. If there are multiple elements with a class of .example-class, this will only toggle the class of the one that is clicked on.
    Notice that $(this) does not require quotes around it, since it is not a CSS class. Instead, this is a JavaScript keyword.
    $(this) behaves just like our other selectors. We can attach toggleClass or toggle to it in the same way.
*/


//NEXT
//jQuery has a function named next to help us select elements that are next to another element. If we have this in our HTML

<div class='item-one'> ... </div>
<div class='item-two'> ... </div>

//If we wanted to hide item-two, we could write:

$('.item-one').next().hide();

/*   We can attach next to any jQuery selector to select the next direct element.
    Then, we can attach any jQuery function to next(). In this case, we attached hide, which would hide the next element after the $('.item-one') element.
*/
//example

function main() {
 $('.skillset').hide();
  $('.skillset').fadeIn(1000);
 // $('.projects').hide();
  $('.projects-button').on('click',function(){
     $(this).next().toggle();
    $(this).toggleClass('active');
  });
  
}

$(document).ready(main);

//TEXT
//We can change the text of an element with the jQuery function text. It's syntax looks like this:

$('.my-selector').text('Hello world!');

 /*  text attaches directly to a jQuery selector.
    Inside of text's parentheses, we can provide text that will become the text of our DOM element. The text we supply will replace any existing text, and if the element has no pre-existing text, text will add it.
*/
//example

function main() {
 $('.skillset').hide();
  $('.skillset').fadeIn(1000);
 // $('.projects').hide();
  $('.projects-button').on('click',function(){
     $(this).next().toggle();
    $(this).toggleClass('active');
    $(this).text('Projects Viewed');
  });
  
}

$(document).ready(main);


//SLIDETOGGLE
//can animate an element's entrance and exit

$('.example-class').slideToggle(400);

/*  slideToggle can be called directly on a jQuery selector.
    slideToggle also takes a parameter of milliseconds that the animation should last. The default is 400 milliseconds, or 0.4 seconds.
*/
//example

function main() {
 $('.skillset').hide();
  $('.skillset').fadeIn(1000);
 // $('.projects').hide();
  $('.projects-button').on('click',function(){
    // $(this).next().toggle();
    $(this).toggleClass('active');
    $(this).text('Projects Viewed');
    $(this).next().slideToggle(400);
  });
  
}

$(document).ready(main);
