//LOOPS
// repeats a task over and over so we don't have to


//FOR-LOOPS
// which let us loop a block of code a known amount of times
//Within the for loop's parentheses is the start condition, stop condition, and iterator 

var animals = ["Grizzly Bear", "Sloth", "Sea Lion"]; 

for (var i = 0; i < animals.length; i++) { //inkrement
	console.log(animals[i]); }

var vacationSpots = ['Paris', 'New York', 'Barcelona'];

for (var i = vacationSpots.length -1; i >= 0; i--){ //dekrement
console.log('I would love to visit ' + vacationSpots[i]);
}

//RUN A FOR-LOOP INSIDE ANOTHER FOR-LOOP

var myPlaces = [1, 2, 3];
var friendPlaces = [1, 2, 3];

for (var i = 0; i < myPlaces.length; i++) {
    console.log(myPlaces[i]);
  
  for (var j = 0; j < friendPlaces.length; j++) {
        console.log(friendPlaces[j]);
    
    if (myPlaces[i] === friendPlaces[j]) {
    			console.log('Match: ' + myPlaces[i]);
				}
    }
}


//WHILE-LOOPS
// which let us loop a block of code an unknown amount of times

while (condition) {
  // code block that loops until condition is false
}

//example
var cards = ['Diamond', 'Spade', 'Heart', 'Club'];

var currentCard = 'Heart';

while (currentCard !== 'Spade') {
  console.log(currentCard);

  var randomNumber = Math.floor(Math.random() * 4);

  currentCard = cards[randomNumber];
}

console.log('Found a Spade!');

