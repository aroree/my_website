//SCOPE
//refers to where in a program a variable can be accessed. The idea is that some variables are unable to be accessed everywhere within a program.
//If you write a variable outside of a function in JavaScript, it's in the global scope and can be used by any other part of the program

var laundryRoom = 'Basement';
var mailRoom = 'Room 1A';


//FUNCTIONAL SCOPE
//When we write variables inside a function, only that function has access to its own variables
//In addition to a function having access to its own variables, it also has access to variables in the global scope

function myApartment(){
  var mailBoxNumber = 'Box 3';
  var laundryRoom = 'In-unit';
  
  console.log('Mail box: ' + mailBoxNumber + ', Laundry:' + laundryRoom);
}

myApartment();
