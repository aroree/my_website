//JACASCRIPT

//building blocks called data types
// 3 essential data types:
	
    //1. String: Any grouping of words or numbers surrounded by single quotes: ' ... ' or double quotes " ... ".
    //2. Number: Any number, including numbers with decimals, without quotes: 4, 8, 1516, 23.42.
    //3. Boolean: This is always one of two words. Either true or false, with no quotations.

var myString = "Aroree";
var myNumber = 9;
var myBoolean = false;


//CONSOLE-OUTPUT

console.log('Your message here.');


//We can log multiple things at once by separating them with commas

console.log('bacon', 'pesto');


//math-operations also possible and will show the result
/*
    Add: +
    Subtract: -
    Multiply: *
    Divide: /  
    Modulus: %  */


console.log(3.5 + 60);


//BUILT-IN FUNCTIONS
//Math-functions
 //generate a random number:
	Math.random();

//generate a number between 0 and 1, multiply it with 50 -> get number between 0 and 50
	Math.random() * 50;

//round the generated number down to the nearest whole number
	Math.floor(Math.random() * 50);

//give output on console
	console.log(Math.floor(Math.random() * 50));



//VARIABLE TYPES

//Variables can hold any data type, like strings, numbers, Booleans, arrays, functions and objects 

 /* var, short for variable, is the JavaScript keyword that will create a new variable for us.
myName is chosen by a developer (that's you!). Notice that the word has no spaces, and each new word is capitalized. This is a common convention in JavaScript, and is called camelCase.
'Arya' is the value that the equals = assigns into the variable myName 
*/
	var myName = 'Arya';
	console.log(myName); //Output Arya


// variables are indeed variable

var weatherCondition = 'Monday: Raining cats and dogs';
weatherCondition = 'Tuesday: Sunny';

console.log(weatherCondition); 


//Interpolation
// use the + operator

	var myPet = 'armadillo';
	console.log('I own a pet ' + myPet + '.'); 
	// Output: 'I own a pet armadillo.'
